// swift-tools-version:4.1

import PackageDescription

let package = Package(
    name: "RequestId",
    products: [
        .library(name: "RequestId", targets: ["RequestId"]),
    ],
    dependencies: [
         // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "RequestId", dependencies: ["Vapor"]),
        .testTarget(name: "RequestIdTests", dependencies: ["RequestId"])
    ]
)
