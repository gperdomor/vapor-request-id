# Request Id

Adds a request id to Vapor 3 responses.

## Installation

Add this project to the `Package.swift` dependencies of your Vapor project:

```swift
.package(url: "https://gitlab.com/gperdomor/vapor-request-id.git", from: "1.0.0")
```

## Setup

Just import the package

```swift
import RequestId
```

and add the middleware to the middleware config of your application

```swift
var middlewareConfig = MiddlewareConfig()
middlewareConfig.use(RequestIdMiddleware())
// Other middlewares
services.register(middlewareConfig)
```

**Note:** You should ensure you set the `RequestIdMiddleware` as the first middleware in your `MiddlewareConfig`.

### Customize

You can override the default configuration. Example:

```swift
let ridConfig = RequestIdMiddleware.Configuration(
    expose: "X-Req-Id",
    header: "X-Req-Id",
    query: "requestId"
)

var middlewareConfig = MiddlewareConfig()
middlewareConfig.use(RequestIdMiddleware(configuration: ridConfig))
// Other middlewares
services.register(middlewareConfig)
```

| Parameter |   Default    | Description                                                                   |
| --------- | :----------: | ----------------------------------------------------------------------------- |
| expose    | X-Request-Id | The name of the header to expose the id on the response                       |
| header    |     nil      | The name of the header to read the id on the request, or `nil` to disable     |
| query     |     nil      | The name of the param to read the id on the query string, or `nil` to disable |

## Credits

This package is developed and maintained by [Gustavo Perdomo](https://gitlab.com/gperdomor).

## License

ResponseTime is released under the [MIT License](LICENSE).
