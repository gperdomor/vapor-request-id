//
//  RequestIdMiddleware.swift
//  Request Id
//
//  Created by Gustavo Perdomo on 09/02/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import Vapor

// swiftlint:disable force_unwrapping

/// Middleware that adds the the request id to the responses.
/// For configuration of this middleware please use the `RequestIdMiddleware.Configuration` object.
///
/// - note: Make sure this middleware is inserted before all your error/abort middlewares,
///         so that even the failed request responses contain proper response id information.
public final class RequestIdMiddleware: Middleware {
    /// Configuration used for populating headers in responses.
    public struct Configuration {
        /// Default configuration.
        ///
        /// - Expose: X-Request-Id
        /// - Header: nil
        /// - Query: nil
        public static func `default`() -> Configuration {
            return .init(
                expose: "X-Request-Id",
                header: nil,
                query: nil
            )
        }

        /// Header name for the header response.
        public let expose: String

        /// Header name for the header request.
        public let header: String?

        /// Query param name for query request.
        public let query: String?

        /// Instantiate a RequestIdConfiguration struct that can be used to create a `RequestIdConfiguration`
        /// middleware for adding support for Request ID in your responses.
        ///
        /// - parameters:
        ///   - expose: The name of the header to expose the id on the response.
        ///   - header: The name of the header to read the id on the request, or `nil` to disable.
        ///   - query: The name of the param to read the id on the query string, or `nil` to disable.
        public init(
            expose: String,
            header: String?,
            query: String?
            ) {
            self.expose = expose
            self.header = header
            self.query = query
        }
    }

    /// Configuration used for populating headers in response.
    public let configuration: Configuration

    /// Creates a Request ID middleware with the specified configuration.
    ///
    /// - parameters:
    ///     - configuration: Configuration used for populating headers in
    ///                      response.
    public init(configuration: Configuration = .default()) {
        self.configuration = configuration
    }

    public func respond(to request: Request, chainingTo next: Responder) throws -> EventLoopFuture<Response> {
        var id: String? = nil

        if let query = self.configuration.query {
            id = request.query[String.self, at: query]
        }

        if id == nil, let header = self.configuration.header {
            id = request.http.headers.firstValue(name: HTTPHeaderName(header))
        }

        if id == nil {
            id = UUID().uuidString
        }

        request.http.headers.replaceOrAdd(name: self.configuration.expose, value: id!)

        let response = try next.respond(to: request)

        return response.map { res in
            res.http.headers.replaceOrAdd(
                name: self.configuration.expose,
                value: id!
            )

            return res
        }
    }
}
