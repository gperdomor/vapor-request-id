//
//  LinuxMain.swift
//  Request Id
//
//  Created by Gustavo Perdomo on 09/02/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import XCTest

import RequestIdTests

var tests = [XCTestCaseEntry]()
tests += RequestIdTests.allTests()
XCTMain(tests)
