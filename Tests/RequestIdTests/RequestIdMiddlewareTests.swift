//
//  RequestIdMiddlewareTests.swift
//  Request Id
//
//  Created by Gustavo Perdomo on 08/29/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

@testable import RequestId

import Vapor
import XCTest

final class RequestIdMiddlewareTests: XCTestCase {
    func testDefaultConfig() throws {
        let config = RequestIdMiddleware.Configuration.default()

        XCTAssertEqual(config.expose, "X-Request-Id")
        XCTAssertNil(config.header)
        XCTAssertNil(config.query)
    }

    func testConfig() throws {
        let config = RequestIdMiddleware.Configuration(expose: "custom-header", header: "req-id", query: "reqId")

        XCTAssertEqual(config.expose, "custom-header")
        XCTAssertEqual(config.header, "req-id")
        XCTAssertEqual(config.query, "reqId")
    }

    func testRespond() throws {
        let requestId = RequestIdMiddleware()
        var services = Services.default()
        var middlewareConfig = MiddlewareConfig()
        middlewareConfig.use(requestId)
        services.register(middlewareConfig)

        let app = try Application(services: services)

        let req = Request(using: app)

        let response = try app.make(Responder.self).respond(to: req).wait()

        XCTAssertTrue(response.http.headers.contains(name: "X-Request-Id"))
    }

    func testUsePassedId() throws {
        let config = RequestIdMiddleware.Configuration(expose: "X-Request-Id", header: "req-id", query: "reqId")

        let requestId = RequestIdMiddleware(configuration: config)
        var services = Services.default()
        var middlewareConfig = MiddlewareConfig()
        middlewareConfig.use(requestId)
        services.register(middlewareConfig)

        let app = try Application(services: services)

        // UUID
        var req = Request(using: app)
        var response = try app.make(Responder.self).respond(to: req).wait()
        XCTAssertTrue(response.http.headers.contains(name: "X-Request-Id"))

        // Header
        req = Request(using: app)
        req.http.headers.replaceOrAdd(name: "req-id", value: "12345")
        response = try app.make(Responder.self).respond(to: req).wait()
        XCTAssertEqual(response.http.headers.firstValue(name: HTTPHeaderName("X-Request-Id")), "12345")

        // Query
        req = Request.init(http: HTTPRequest.init(method: .GET, url: "https://google.com?reqId=98765"), using: app)
        response = try app.make(Responder.self).respond(to: req).wait()
        XCTAssertEqual(response.http.headers.firstValue(name: HTTPHeaderName("X-Request-Id")), "98765")
    }

    // MARK: Linux Helper

    /// Check Linux Tests
    func testLinuxTestSuiteIncludesAllTests() throws {
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
        let thisClass = type(of: self)
        let linuxCount = thisClass.allTests.count
        let darwinCount = Int(thisClass.defaultTestSuite.testCaseCount)
        XCTAssertEqual(linuxCount, darwinCount, "\(darwinCount - linuxCount) tests are missing from allTests")
        #endif
    }

    static var allTests = [
        ("testLinuxTestSuiteIncludesAllTests", testLinuxTestSuiteIncludesAllTests),
        ("testDefaultConfig", testDefaultConfig),
        ("testConfig", testConfig),
        ("testRespond", testRespond),
        ("testUsePassedId", testUsePassedId)
    ]
}
