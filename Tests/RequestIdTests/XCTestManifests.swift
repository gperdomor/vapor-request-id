//
//  XCTestManifests.swift
//  Request Id
//
//  Created by Gustavo Perdomo on 09/02/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(RequestIdMiddlewareTests.allTests)
    ]
}
#endif
